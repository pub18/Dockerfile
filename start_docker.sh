# # start apache2
docker run -p 18080:80  --hostname apache2 --name apache2 --restart always -v /home/seeobject/Dockerfile:/var/www/html/ubuntu -itd apache2:v1.0

# # start docker hub
# docker run -d -p 5000:5000 --name docker-hub -v /data/opt/registry:/var/lib/registry --restart always registry:latest

# #start gitlab
# docker run -d -p 443:443 -p 80:80 -p 2222:22 \
#     --hostname gitlab --name gitlab --restart always \
#     -v /data/gitlab/config:/etc/gitlab \
#     -v /data/gitlab/logs:/var/log/gitlab \
#     -v /data/gitlab/data:/var/opt/gitlab \
#     gitlab/gitlab-ce:latest

# #start mysql
# docker run -d -p 3306:3306 --name mysql --hostname mysql \
#     -e MYSQL_ROOT_PASSWORD=123456 \
#     -e LANG=C.UTF-8 \
#     -v /home/zqp/.mysql/conf:/etc/mysql/conf.d \
#     -v /home/zqp/.mysql/logs:/logs \
#     -v /home/zqp/.mysql/data:/var/lib/mysql \
#     mysql:5.7


#start ubuntu16.04
docker run -itd  --name zqp --hostname root \
    -e LANG=C.UTF-8 \
    --shm-size="2g" \
    -v ${HOME}/data:/data \
    base:16.04

# docker run --name zqp_2204 --hostname root -itd --restart always --gpus all -v /data:/data -v /dcache:/dcache --security-opt seccomp=unconfined --cap-add=SYS_PTRACE --shm-size="20g" ubuntu:22.04
# docker exec -it zhangqipeng /bin/bash
# docker run --name zqp_2204 --hostname root -itd --restart always -p 10022:22 -p  28443:8443  -p 28080:8080 --runtime=nvidia -e NVIDIA_VISIBLE_DEVICES=all -v /data:/data -v /dcache:/dcache --security-opt seccomp=unconfined --cap-add=SYS_PTRACE --shm-size="20g" ubuntu:22.04 /bin/bash

# docker run --name zqp_2204 --hostname root -itd --restart always --gpus all  -p 10022:22 -p  28443:8443  -p 8080:8080 --shm-size="20g" ubuntu20.04:v1.1

docker run -p 18080:80  --hostname root --name ocr --restart always --gpus all  -itd pytorch-1.10.0/cu113:1.0.1 /bin/bash
docker run --hostname root --name ocr --restart always --gpus all -itd pytorch-1.10.0/cu113:1.0.0