FROM ubuntu:22.04
WORKDIR  /root
# SHELL ["/bin/bash", "-c"]
ENV LANG=C.UTF-8 DEBIAN_FRONTEND=noninteractive 
RUN sed -i "s@http://.*archive.ubuntu.com@http://mirrors.huaweicloud.com@g" /etc/apt/sources.list && \
    sed -i "s@http://.*security.ubuntu.com@http://mirrors.huaweicloud.com@g" /etc/apt/sources.list && \
    sed -i -e 's/^APT/# APT/' -e 's/^DPkg/# DPkg/' /etc/apt/apt.conf.d/docker-clean && \
    apt update && apt install -y apt-transport-https vim openssh-server gcc g++ cmake git gdb net-tools inetutils-ping ccache && \
    sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -i 's/^#\(PermitRootLogin.*\)/\1/' /etc/ssh/sshd_config && \
    echo "root:root" | chpasswd && \
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ./miniconda.sh && \
    bash ./miniconda.sh -b -p /opt/miniconda3 && /opt/miniconda3/bin/conda init bash && rm ./miniconda.sh && \
    /opt/miniconda3/bin/conda config --set show_channel_urls yes  && \
    /opt/miniconda3/bin/conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/  && \
    /opt/miniconda3/bin/conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/  && \
    /opt/miniconda3/bin/conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge   && \
    /opt/miniconda3/bin/conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/  && \
    /opt/miniconda3/bin/conda clean -i -y && /opt/miniconda3/bin/conda create -n py38 python=3.8 -y && echo 'conda activate py38' >> ~/.bashrc && \
    . ~/.bashrc && pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple && \
    pip install jupyter notebook && \
    printf 'service ssh start\nnohup /opt/miniconda3/envs/py38/bin/python3.8 /opt/miniconda3/envs/py38/bin/jupyter-notebook --allow-root --ServerApp.ip=0.0.0.0 --ServerApp.port=8080 &\n/bin/bash' > /bin/start-ssh && \
    wget https://github.com/accellera-official/systemc/archive/refs/tags/2.3.3.tar.gz -O systemc-2.3.3.tar.gz && \
    rm -rf systemc systemc-2.3.3 && tar xzvf systemc-2.3.3.tar.gz && \
    cd systemc-2.3.3 && mkdir build && cd build && \
    ../configure --prefix=/opt/systemc CXXFLAGS=-std=c++14 && \
    make -j 16 && make install && rm -rf systemc* && \
    echo "export SYSTEMC_HOME=/opt/systemc" >> ~/.bashrc && \
    echo "export SYSTEMC_INCLUDE=$SYSTEMC_HOME/include" >> ~/.bashrc && \
    echo "export SYSTEMC_LIBDIR=$SYSTEMC_HOME/lib-linux64" >> ~/.bashrc && \
    rm -rf /root/*

CMD ["/bin/bash", "/bin/start-ssh"]
